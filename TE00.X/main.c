/*
 * File:   main.c
 * Author: L. Daniel Loaiza E
 *
 * Created on 3 de julio de 2019, 08:00 PM
 */



#include<main.h>


typedef uint8_t u8;
typedef int32_t i32;

u8 UART1_TX_buffer[100];
const i32 var_in_flash=10;


void main(void) {    
    i32 var_for_print=8;
    UART1_init(115200);
    UART1_write_string("Pint to console... \n");
    sprintf(UART1_TX_buffer, "message to print with %d \n", var_for_print);
UART1_write_string(UART1_TX_buffer);
 sprintf(UART1_TX_buffer, "print from the flash memory: %d \n", var_in_flash);
UART1_write_string(UART1_TX_buffer);


//turn on led 1 in the curiosity
mPORTESetPinsDigitalOut(BIT_4);
mPORTESetBits(BIT_4);

    while(1){};
}